package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person implements Observer {
    private Set<Klass> assignedClasses = new HashSet<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.introduceWords += " I am a teacher.";
    }

    public void assignTo(Klass klass) {
        if (klass == null) return;
        this.assignedClasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        if (klass == null) return false;
        return this.assignedClasses.contains(klass);
    }

    @Override
    public String introduce() {
        if (assignedClasses.size() != 0) {
            return this.introduceWords + " I teach Class " +
                    assignedClasses.stream().map(Klass::getNumber).collect(Collectors.toList()).toString()
                    .replace("]","").replace("[","") + ".";
        }
        return this.introduceWords;
    }

    public boolean isTeaching(Student student) {
        if (student == null) return false;
        return this.assignedClasses.contains(student.getKlass());
    }

    @Override
    public void update(Observable o, Object arg) {
        Klass klass = (Klass) arg;
        String teacherName = klass.getAttachedTeacher().getName();
        int classNumber = klass.getNumber();
        String classLeaderName = klass.getClassLeader().getName();
        System.out.format("I am %s, teacher of Class %d. I know %s become Leader.", teacherName, classNumber, classLeaderName);
    }
}
