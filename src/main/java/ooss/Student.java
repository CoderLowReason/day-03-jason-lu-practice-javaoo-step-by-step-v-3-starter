package ooss;

import java.util.Observable;
import java.util.Observer;

public class Student extends Person implements Observer {

    private Klass klass;

    public Klass getKlass() {
        return klass;
    }

    public Student(int id, String name, int age) {
        super(id, name, age);
        this.introduceWords += " I am a student.";
    }

    public void join(Klass klass) {
        if (klass == null) return;
        this.klass = klass;
    }

    @Override
    public String introduce() {
        if (klass != null) {
            if (this.equals(klass.getClassLeader())) {
                return this.introduceWords + " I am the leader of class " + this.klass.getNumber() + ".";
            }
            return this.introduceWords + " I am in class " + this.klass.getNumber() + ".";
        }
        return this.introduceWords;
    }

    public boolean isIn(Klass klass) {
        if (klass == null) return false;
        return klass.equals(this.klass);
    }

    @Override
    public void update(Observable o, Object arg) {
        Klass klass = (Klass) arg;
        String studentName = klass.getAttachedStudent().getName();
        int classNumber = klass.getNumber();
        String classLeaderName = klass.getClassLeader().getName();
        System.out.format("I am %s, student of Class %d. I know %s become Leader.", studentName, classNumber, classLeaderName);
    }
}
